Write-Host "Cleaning TypeResolver..."
dotnet clean -c Release .\TypeResolver.sln | Out-Null

Write-Host "Running TypeResolver Tests..."
$output = (dotnet test -c Release .\TestTypeResolver\TestTypeResolver.csproj)
$expectedPassCount = 2
$passCount = ([Regex]::Matches($output, "Passed!")).Count
if ($passCount -ne $expectedPassCount) {
  throw "Test failures! Aborting release process."
}
Write-Host "All Tests passed!" -ForegroundColor Green

Write-Host "Packing TypeResolver..."
dotnet clean -c Release .\TypeResolver.sln | Out-Null
dotnet pack -c Release -p:ContinuousIntegrationBuild=true .\TypeResolver\TypeResolver.csproj | Out-Null

Write-Host "Nuget packed into Packages\Release!" -ForegroundColor Green
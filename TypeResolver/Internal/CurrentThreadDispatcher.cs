﻿using System;
using System.Linq.Expressions;
using System.Threading;

namespace TypeResolver.Internal
{
    internal sealed class CurrentThreadDispatcher
    {
        private static Type _dispatcherType;
        private static Action<object> _invokeShutdown;
        private static Func<Thread, object> _getDispatcherFromThread;

        public static void ShutdownIfNecessary( ) {
            if ( GetDispatcherFromThread( ) is object dispatcher ) {
                InvokeShutdown( dispatcher );
            }
        }

        private static object GetDispatcherFromThread( ) {
            if ( _getDispatcherFromThread is object ) {
                return _getDispatcherFromThread( Thread.CurrentThread );
            }

            if ( _dispatcherType is null ) {
                _dispatcherType = Type.GetType( "System.Windows.Threading.Dispatcher,WindowsBase" );
            }
            if ( _dispatcherType is null ) {
                return null;
            }

            var fromThreadMethodInfo = _dispatcherType.GetMethod( "FromThread" );
            _getDispatcherFromThread = ( Func<Thread, object> )Delegate.CreateDelegate( typeof( Func<Thread, object> ), fromThreadMethodInfo );
            return GetDispatcherFromThread( );
        }

        private static void InvokeShutdown( object dispatcher ) {
            if ( _invokeShutdown is object ) {
                _invokeShutdown( dispatcher );
                return;
            }

            // Cannot use `CreateDelegate` here because we cannot create a strongly typed delegate referencing `Dispatcher`.  So,
            // instead, we use Linq expresssions to get a fast delegate we can call to invoke shutdown.
            var invokeShutdownMethodInfo = _dispatcherType.GetMethod( "InvokeShutdown" );
            var parameterExpression = Expression.Parameter( typeof( object ), "dispatcher" );
            var castedDispatcherInstanceExpression = Expression.Convert( parameterExpression, _dispatcherType );
            var callExpression = Expression.Call( castedDispatcherInstanceExpression, invokeShutdownMethodInfo );
            _invokeShutdown = Expression.Lambda<Action<object>>( callExpression, parameterExpression ).Compile( );

            InvokeShutdown( dispatcher );
        }
    }
}

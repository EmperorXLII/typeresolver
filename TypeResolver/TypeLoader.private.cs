﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using TypeResolver.Internal;
using Xunit.Extensions;


namespace TypeResolver {
    using AssemblyCache = System.Collections.Concurrent.ConcurrentDictionary<Assembly, IEnumerable<Assembly>>;
    using TypeCache = System.Collections.Concurrent.ConcurrentDictionary<Assembly, IEnumerable<Type>>;

    public static partial class TypeLoader {

#if NET5_0_OR_GREATER
        private const string CoreRuntimeLibraryName = "System.Private.CoreLib,";
#else
        private const string CoreRuntimeLibraryName = "mscorlib,";
#endif

        private static readonly TypeCache types_ = new TypeCache( );
        private static readonly HashSet<string> excludedAssemblies_ = new HashSet<string>( StringComparer.OrdinalIgnoreCase ) { "xunit.runner.", "xunit.execution.", "xunit.abstractions." };
        private static readonly AssemblyCache referencedAssemblies_ = new AssemblyCache( );
        private static readonly AssemblyCache referringAssemblies_ = new AssemblyCache( );

        internal static readonly string TypeLoaderAssemblyKey = typeof( TypeLoader ).FullName + " Loading Assembly";


        private static bool IsExcludedAssembly( Assembly assembly ) {
            return IsExcludedAssembly( assembly.FullName );
        }

        private static bool IsExcludedAssembly( string assemblyFullName ) {
            return excludedAssemblies_.Contains( assemblyFullName )
                || excludedAssemblies_.Any( excluded => assemblyFullName.StartsWith( excluded, StringComparison.OrdinalIgnoreCase ) );
        }

        private static IEnumerable<Assembly> GetReferencedAssemblies( Assembly assembly ) {
            if( !referencedAssemblies_.ContainsKey( assembly ) ) {
                var referencedAssemblyLoadCanFail = ReferencedAssemblyLoadCanFail( assembly );
                try {
                    referencedAssemblies_[assembly] =
                        IsExcludedAssembly( assembly )
                            ? new Assembly[0]
                            : assembly.GetReferencedAssemblies( )
                                      .Where( assemblyName => !IsExcludedAssembly( assemblyName.FullName ) )
                                      .Select( name => LoadAssembly( name, referencedAssemblyLoadCanFail ) )
                                      .Where( loaded => loaded != null )
                                      .ToArray( );

                    ProcessExclusionAttributes( referencedAssemblies_[assembly] );
                }
                catch( Exception ex ) {
                    // Include assembly that caused the failure.
                    ex.Data[TypeLoaderAssemblyKey] = assembly;
                    throw;
                }
            }

            return referencedAssemblies_[assembly];
        }

        private static Assembly LoadAssembly( AssemblyName assemblyName, bool referencedAssemblyLoadCanFail ) {
            try {
                return Assembly.Load( assemblyName );
            }
            catch when ( referencedAssemblyLoadCanFail ) {
                return null;
            }
        }

        private static bool ReferencedAssemblyLoadCanFail( Assembly assembly ) {
#if NET5_0_OR_GREATER
            // In .NET (Core), many of the system assemblies have references to things that
            // come from non-system nugets.  If you try to use an assembly like this when
            // developing, and you try to use a feature that requires that reference, you'll
            // be prompted to add the right nuget package.  See more information here:
            // https://github.com/dotnet/standard/tree/master/docs/planning/netstandard-2.0
            // Because of this, we're going to allow Assembly.Load failures for any assembly
            // that's not in the AppContext.BaseDirectory (output directory).  We use this as
            // a proxy for "system" assemblies.  If it's not in the BaseDirectory, it's a
            // "system" assembly, and we'll allow its references to fail to load.
            var baseDirectory = AppContext.BaseDirectory;
            string assemblyDirectory;
            try {
                assemblyDirectory = Path.GetDirectoryName( assembly.Location );
            }
            catch ( NotSupportedException ) {
                return true;
            }

            if ( string.IsNullOrEmpty( assemblyDirectory ) ) {
                return true;
            }

            var comparison = OperatingSystem.IsWindows( ) ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal;
            var normalizedBaseDirectory = Path.GetFullPath( baseDirectory );
            var normalizedAssemblyDirectory = Path.GetFullPath( assemblyDirectory );
            var assemblyIsInBaseDirectory = normalizedAssemblyDirectory.StartsWith( normalizedBaseDirectory, comparison );

            return !assemblyIsInBaseDirectory;
#else
            return false;
#endif
        }

        private static IEnumerable<Assembly> GetReferringAssemblies( Assembly referenceAssembly ) {
            if( !TypeLoader.referringAssemblies_.ContainsKey( referenceAssembly ) ) {
                // Retrieve currently loaded assemblies.
                var allAssemblies = new List<Assembly>( TypeLoader.referringAssemblies_.Keys );
                allAssemblies.AddRange( AppDomain.CurrentDomain.GetAssemblies( ) );

                // Check for excluded assemblies, before processing references.
                ProcessExclusionAttributes( allAssemblies );

                // Retrieve all referenced assemblies.
                for( int i = 0; i < allAssemblies.Count; ++i ) {
                    var assembly = allAssemblies[i];
                    foreach( var reference in GetReferencedAssemblies( assembly ) ) {
                        if( !allAssemblies.Contains( reference ) )
                            allAssemblies.Add( reference );
                    }
                }

                foreach( var assembly in allAssemblies ) {
                    var assemblyName = assembly.FullName;
                    var referrers =
                        IsExcludedAssembly( assemblyName )
                            ? new Assembly[0].AsEnumerable( )
                            : allAssemblies
                                .Where( ( a ) => TypeLoader.DoesAssemblyUseTargetAssembly( a, assemblyName ) )
                                .ToReadOnlyCollection( );
                    TypeLoader.referringAssemblies_[assembly] = referrers;
                }
            }

            return referenceAssembly.FullName.StartsWith( CoreRuntimeLibraryName )
                 ? TypeLoader.referringAssemblies_.Keys
                 : TypeLoader.referringAssemblies_[referenceAssembly];
        }

        private static IEnumerable<Type> GetTypes( Assembly assembly ) {
            if( !types_.ContainsKey( assembly ) ) {
                try {
                    types_[assembly] =
                        IsExcludedAssembly( assembly )
                            ? Type.EmptyTypes
                            : GetTypesCore( assembly );
                }
                catch( Exception ex ) {
                    if( ex is NotSupportedException && ex.Message.Contains( "dynamic" ) ) {
                        // Ignore types from dynamic assemblies.
                        TypeLoader.types_[assembly] = Type.EmptyTypes;
                    }
                    else {
                        // Include assembly that caused the failure.
                        ex.Data[TypeLoaderAssemblyKey] = assembly;
                        throw;
                    }
                }
            }

            return TypeLoader.types_[assembly];
        }

        private static IEnumerable<Type> GetTypesCore( Assembly assembly ) {
            // When types are requested, add to type cache.
            var usableTypes = GetExportedTypes( assembly )
                .Where( TypeLoader.IsUsableType )
                .ToReadOnlyCollection( );

            ProcessExclusionAttributes( assembly );

            return usableTypes;
        }

        private static IEnumerable<Type> GetExportedTypes( Assembly assembly ) {
            Type[] allTypes;
            try {
                allTypes = assembly.GetTypes( );
            }
            catch ( ReflectionTypeLoadException exception ) {
                // Some types failed to load - we'll just take the ones that did load:
                allTypes = exception.Types;
            }
            return allTypes.Where( t => t?.IsVisible ?? false );
        }

        private static void ProcessExclusionAttributes( IEnumerable<Assembly> assemblies ) {
            var orderedAssemblies = assemblies.OrderByDescending( assembly => {
                string assemblyName = assembly.FullName;
                return assemblyName.StartsWith( "System" )
                    || assemblyName.StartsWith( "Microsoft" )
                     ? "Z" + assemblyName
                     : assemblyName;
            } );

            foreach( Assembly assembly in orderedAssemblies ) {
                if( !IsExcludedAssembly( assembly ) )
                    ProcessExclusionAttributes( assembly );
            }
        }

        private static void ProcessExclusionAttributes( Assembly assembly ) {
            // Check for invocation helpers.
            var assistAttributes = assembly.GetCustomAttributes( typeof( Xunit.Extensions.InstantiateInstanceDataAttribute ), inherit: false );
            foreach( Xunit.Extensions.InstantiateInstanceDataAttribute attribute in assistAttributes )
                InstanceCreator.InstanceDataInvoker = (IInstantiateInstanceData)Activator.CreateInstance( attribute.InstantiatorType );


            // Check for excluded types.
            var limitAttributes = assembly.GetCustomAttributes( typeof( Xunit.Extensions.LimitInstanceDataAttribute ), inherit: false );
            foreach( Xunit.Extensions.LimitInstanceDataAttribute attribute in limitAttributes )
                TypeCreator.LimitInstances( attribute.TargetType, attribute.AvailableTypes );


            // Check for excluded assemblies.
            var excludeAttributes = assembly.GetCustomAttributes( typeof( Xunit.Extensions.ExcludeAssemblyAttribute ), inherit: false );
            foreach( Xunit.Extensions.ExcludeAssemblyAttribute attribute in excludeAttributes )
                excludedAssemblies_.Add( attribute.AssemblyName );

            // Reset any excluded examined assemblies.
            foreach( Assembly excluded in types_.Keys.Where( IsExcludedAssembly ).ToArray( ) )
                types_[excluded] = Type.EmptyTypes;
        }

        private static bool DoesAssemblyUseTargetAssembly( Assembly assembly, string targetAssemblyName ) {
            Debug.Assert( assembly != null );
            Debug.Assert( targetAssemblyName != null );

            // Check if assembly is the target assembly,
            //  or if it directly references the target assembly.
            return assembly.FullName.OrdinalEqual( targetAssemblyName )
                || GetReferencedAssemblies( assembly )
                    .Any( ( r ) => r.FullName.OrdinalEqual( targetAssemblyName ) );
        }

        private static IEnumerable<Assembly> GetUsableAssemblies( IEnumerable<Assembly> referenceAssemblies ) {
            var referrers = referenceAssemblies
                .Select( GetReferringAssemblies )
                .Aggregate( ( current, next ) => current.Intersect( next ) );
            return referrers;
        }

    }

}
